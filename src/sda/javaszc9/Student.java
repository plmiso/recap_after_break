package sda.javaszc9;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class Student implements Comparable<Student> {

    private String name;
    private String lastName;
    private Integer studentId;
    private List<Classes> attendance;
    private Map<Classes, Integer> grades;

    public Student(String name, String lastName, Integer studentId, List<Classes> attendance, Map<Classes, Integer> grades) {
        this.name = name;
        this.lastName = lastName;
        this.studentId = studentId;
        this.attendance = attendance;
        this.grades = grades;
    }

    public void addGrade(Classes subject, int grade){
        if(attendance.contains(subject)){
            grades.put(subject, grade);
        }else{
            System.out.println("Student don't attend classes");
        }
    }

    public void displayAttendance(){
        for (Classes temp : attendance){
            System.out.println(temp);
        }
    }

    public Double getAverage(){
        Double temp = 0.0;
        for (Integer grade : grades.values()){
            temp = temp + grade;
        }
        return temp = temp / grades.values().size();
    }

    public List<Classes> classesWithoutGrade(){
        Set<Classes> tempSet = grades.keySet();
        List<Classes> temp = new ArrayList<>();
        for (Classes clazz : tempSet){
            if(grades.get(clazz) == null){
                temp.add(clazz);
            }
        }
        return temp;
    }

    @Override
    public int compareTo(Student o) {
        return name.compareTo(o.getName());
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", studentId=" + studentId +
                ", attendance=" + attendance +
                ", grades=" + grades +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public List<Classes> getAttendance() {
        return attendance;
    }

    public void setAttendance(List<Classes> attendance) {
        this.attendance = attendance;
    }

    public Map<Classes, Integer> getGrades() {
        return grades;
    }

    public void setGrades(Map<Classes, Integer> grades) {
        this.grades = grades;
    }
}
