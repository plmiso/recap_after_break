package sda.javaszc9;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum Classes {

    BIOLOGY,
    BIOLOGY_LAB,
    MATH,
    MATH_LAB,
    PHYSICAL_EDUCATION,
    PHILOSOPHY,
    PHILOSOPHY_LAB;

    private static final List<Classes> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static Classes randomClass(){
        return VALUES.get(RANDOM.nextInt(SIZE));
    }


}
