package sda.javaszc9;

import java.util.*;

public class Main {

    static Random random;

    public static void main(String[] args) {
        random = new Random();
        List<Student> studentList = generateSomeStudents();
        addSomeGrades(studentList);
        studentList.forEach(student -> System.out.println(student.toString()));

        System.out.println("PRZED SORTOWANIEM");

        Collections.shuffle(studentList);
        studentList.forEach(student -> System.out.println(student.toString()));

        System.out.println("PO SORTOWANIU PO NAZWISKU");

        Collections.sort(studentList);
        studentList.forEach(student -> System.out.println(student.toString()));

        Student student = studentList.get(0);
        System.out.println(student);

        student.displayAttendance();
        student.classesWithoutGrade().forEach(System.out::println);

    }

    private static List<Student> generateSomeStudents(){

        List<Student> tempListOfStudents = new ArrayList<>();
        List<Classes> tempListOfClasses;
        for (int i = 0; i <10 ; i++) {
            tempListOfClasses = new ArrayList<>();
            for (int j = 0; j <3 ; j++) {
                tempListOfClasses.add(Classes.randomClass());
            }
            Student student = new Student("StudentName" + i, "StudentSurname" + i, random.nextInt(10000) + 9999, tempListOfClasses,new HashMap<>());
            tempListOfStudents.add(student);
        }
        return tempListOfStudents;
    }

    private static void addSomeGrades(List<Student> studentList){
        for(Student student : studentList){
            List<Classes> studentAttendance = student.getAttendance();
            for (int i = 0; i < 4; i++) {
                student.addGrade(studentAttendance.get(random.nextInt(studentAttendance.size())), random.nextInt(3) + 3);
            }
        }
    }



}
